package com.espread;
import org.eclipse.jetty.server.Server;
import com.espread.jetty.JettyFactory;
import com.espread.jetty.Profiles;

/**
 * 使用Jetty运行调试Web应用, 在Console输入回车快速重新加载应用.
 */
class StartServerCore {

	public static final int PORT = 9090;
	public static final String CONTEXT = "/espread";
	public static final String[] TLD_JAR_NAMES = new String[] {"spring-webmvc", "shiro-web"};

	public static void main(String[] args) throws Exception {
		// 设定Spring的profile环境变量
		Profiles.setProfileAsSystemProperty(Profiles.DEVELOPMENT);

		// 启动Jetty
		Server server = JettyFactory.createServerInSource(PORT, CONTEXT);
		JettyFactory.setTldJarNames(server, TLD_JAR_NAMES);

		try {
			server.start();

			System.out.println("[INFO] 服务已启动：http://localhost:" + PORT + CONTEXT);
			System.out.println("[HINT] 按回车快速重启服务.....");

			// 等待用户输入回车重载应用.
			while (true) {
				char c = (char) System.in.read();
				if (c == '\n') {
					JettyFactory.reloadContext(server);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
}
